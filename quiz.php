<?php include_once 'header.php'; ?>

    <div class="play_area">
        <div class="container">
            <div id="play_slider" class="play_slider">
                <div>
                    <div class="play_item">
                        <div class="play_wrapper">
                            <div class="play_header">
                                <div>
                                    <div class="play_buttons left">
                                        <a class="play_btn js_share" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']; ?>" title="Ask a friend" target="_blank"><i class="fa fa-facebook"></i></a>
                                        <a class="play_btn js_get_hint" href="#hints" title="Get a hint"><i class="fa fa-info"></i></a>
                                    </div>
                                </div>
                                <div>
                                    <div id="question_wrapper" class="question_wrapper text-center">
                                        Care este valoarea impoozitului pe venit?
                                    </div>
                                </div>
                                <div>
                                    <div class="play_buttons right">
                                        <a class="play_btn js_timer active" href="#" title="Timer"><span id="play_timer">00:30</span></a>
                                        <a class="play_btn js_again" href="#" title="Try again"><i class="fa fa-refresh"></i></a>
                                    </div>
                                </div>
                            </div>

                            <div class="play_body">
                                <ul class="options" data-id="321">
                                    <li class="option" data-value="0">12%</li>
                                    <li class="option" data-value="1">15%</li>
                                    <li class="option" data-value="2">17,5%</li>
                                </ul>
                            </div>
                        </div>

                        <div class="container">
                            <div class="section play_meta">
                                <div class="play_success">
                                    <div class="info success hide">
                                        <p>
                                            <img class="dog" src="images/dog.png" alt="">
                                        </p>

                                        <h3><strong><i class="fa fa-exclamation-triangle"></i></strong>  Excelent!</h3>

                                        <p>
                                            Ești un <strong>geniu!</strong>. Trecem la <br> următoarea întrebare ?
                                        </p>

                                        <p>
                                            <a class="tap js_next_question" href="#next">Da, hai la următoarea <i class="fa fa-long-arrow-right"></i></a>
                                        </p>
                                    </div>
                                </div>
                                <div class="play_errors">
                                    <div class="info error hide">
                                        <p>
                                            <img class="dog" src="images/dog.png" alt="">
                                        </p>

                                        <h3><strong><i class="fa fa-exclamation-triangle"></i></strong>  Ai fost aproape!</h3>

                                        <p>
                                            Utilizează <strong>butoanele de ajutor</strong> sau <br> mergi la următarea întrebare.
                                        </p>

                                        <p>
                                            <a class="tap js_next_question" href="#next">Următoarea întrebare <i class="fa fa-long-arrow-right"></i></a>
                                        </p>
                                    </div>
                                </div>
                                <div class="play_hints">
                                    <div class="hint hide">
                                        <h3><strong><i class="fa fa-info-circle"></i></strong> Hint</h3>
                                        <hr>
                                        <p>
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo impedit iure maxime
                                            pariatur quisquam sint voluptate. Asperiores commodi consectetur corporis cupiditate
                                            fugit iure laborum, minima mollitia odio quae, sapiente vitae!
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="play_item">
                        <div class="play_wrapper">
                            <div class="play_header">
                                <div>
                                    <div class="play_buttons left">
                                        <a class="play_btn js_share" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']; ?>" title="Ask a friend" target="_blank"><i class="fa fa-facebook"></i></a>
                                        <a class="play_btn js_get_hint" href="#hints" title="Get a hint"><i class="fa fa-info"></i></a>
                                    </div>
                                </div>
                                <div>
                                    <div id="question_wrapper" class="question_wrapper text-center">
                                        Care este valoarea impoozitului pe venit? 2
                                    </div>
                                </div>
                                <div>
                                    <div class="play_buttons right">
                                        <a class="play_btn js_timer active" href="#" title="Timer"><span id="play_timer">00:30</span></a>
                                        <a class="play_btn js_again" href="#" title="Try again"><i class="fa fa-refresh"></i></a>
                                    </div>
                                </div>
                            </div>

                            <div class="play_body">
                                <ul class="options" data-id="321">
                                    <li class="option" data-value="0">12%</li>
                                    <li class="option" data-value="1">15%</li>
                                    <li class="option" data-value="2">17,5%</li>
                                </ul>
                            </div>
                        </div>

                        <div class="container">
                            <div class="section play_meta">
                                <div class="play_success">
                                    <div class="info success hide">
                                        <p>
                                            <img class="dog" src="images/dog.png" alt="">
                                        </p>

                                        <h3><strong><i class="fa fa-exclamation-triangle"></i></strong>  Excelent!</h3>

                                        <p>
                                            Ești un <strong>geniu!</strong>. Trecem la <br> următoarea întrebare ?
                                        </p>

                                        <p>
                                            <a class="tap js_next_question" href="#next">Da, hai la următoarea <i class="fa fa-long-arrow-right"></i></a>
                                        </p>
                                    </div>
                                </div>
                                <div class="play_errors">
                                    <div class="info error hide">
                                        <p>
                                            <img class="dog" src="images/dog.png" alt="">
                                        </p>

                                        <h3><strong><i class="fa fa-exclamation-triangle"></i></strong>  Ai fost aproape!</h3>

                                        <p>
                                            Utilizează <strong>butoanele de ajutor</strong> sau <br> mergi la următarea întrebare.
                                        </p>

                                        <p>
                                            <a class="tap js_next_question" href="#next">Următoarea întrebare <i class="fa fa-long-arrow-right"></i></a>
                                        </p>
                                    </div>
                                </div>
                                <div class="play_hints">
                                    <div class="hint hide">
                                        <h3><strong><i class="fa fa-info-circle"></i></strong> Hint</h3>
                                        <hr>
                                        <p>
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo impedit iure maxime
                                            pariatur quisquam sint voluptate. Asperiores commodi consectetur corporis cupiditate
                                            fugit iure laborum, minima mollitia odio quae, sapiente vitae!
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="play_item">
                        <div class="play_wrapper">
                            <div class="play_header">
                                <div>
                                    <div class="play_buttons left">
                                        <a class="play_btn js_share" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']; ?>" title="Ask a friend" target="_blank"><i class="fa fa-facebook"></i></a>
                                        <a class="play_btn js_get_hint" href="#hints" title="Get a hint"><i class="fa fa-info"></i></a>
                                    </div>
                                </div>
                                <div>
                                    <div id="question_wrapper" class="question_wrapper text-center">
                                        Care este valoarea impoozitului pe venit? 3
                                    </div>
                                </div>
                                <div>
                                    <div class="play_buttons right">
                                        <a class="play_btn js_timer active" href="#" title="Timer"><span id="play_timer">00:30</span></a>
                                        <a class="play_btn js_again" href="#" title="Try again"><i class="fa fa-refresh"></i></a>
                                    </div>
                                </div>
                            </div>

                            <div class="play_body">
                                <ul class="options" data-id="321">
                                    <li class="option" data-value="0">12%</li>
                                    <li class="option" data-value="1">15%</li>
                                    <li class="option" data-value="2">17,5%</li>
                                </ul>
                            </div>
                        </div>

                        <div class="container">
                            <div class="section play_meta">
                                <div class="play_success">
                                    <div class="info success hide">
                                        <p>
                                            <img class="dog" src="images/dog.png" alt="">
                                        </p>

                                        <h3><strong><i class="fa fa-exclamation-triangle"></i></strong>  Excelent!</h3>

                                        <p>
                                            Ești un <strong>geniu!</strong>. Trecem la <br> următoarea întrebare ?
                                        </p>

                                        <p>
                                            <a class="tap js_next_question" href="#next">Da, hai la următoarea <i class="fa fa-long-arrow-right"></i></a>
                                        </p>
                                    </div>
                                </div>
                                <div class="play_errors">
                                    <div class="info error hide">
                                        <p>
                                            <img class="dog" src="images/dog.png" alt="">
                                        </p>

                                        <h3><strong><i class="fa fa-exclamation-triangle"></i></strong>  Ai fost aproape!</h3>

                                        <p>
                                            Utilizează <strong>butoanele de ajutor</strong> sau <br> mergi la următarea întrebare.
                                        </p>

                                        <p>
                                            <a class="tap js_next_question" href="#next">Următoarea întrebare <i class="fa fa-long-arrow-right"></i></a>
                                        </p>
                                    </div>
                                </div>
                                <div class="play_hints">
                                    <div class="hint hide">
                                        <h3><strong><i class="fa fa-info-circle"></i></strong> Hint</h3>
                                        <hr>
                                        <p>
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo impedit iure maxime
                                            pariatur quisquam sint voluptate. Asperiores commodi consectetur corporis cupiditate
                                            fugit iure laborum, minima mollitia odio quae, sapiente vitae!
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php include_once 'footer.php'; ?>