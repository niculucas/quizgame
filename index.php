<?php include_once 'header.php'; ?>

    <div class="container">
        <div class="section">
           <div class="welcome">
               <img class="dog" src="images/dog.png" alt="">
               <h3>
                   Bine ai venit la <strong>Quiz Game!</strong>
               </h3>

               <p>
                   Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur consequuntur deleniti
                   explicabo illo odio qui quia quod sunt. Aliquid doloribus facere fuga illum molestiae optio
                   perspiciatis similique soluta vel vitae.
               </p>

               <div class="spacing"></div>

               <h3>
                   Alege o categorie pentru a incepe
               </h3>
           </div>

            <a href="quiz.php" class="card_box">
                <div class="full_width">
                    <div class="card_name">
                        <h3>Level 1</h3>
                    </div>
                </div>
                <div class="half_width">
                    <div class="card_points">
                        <span>points</span>
                        21
                    </div>
                </div>
                <div class="half_width">
                    <div class="card_questions">
                        <span>questions</span>
                        10/30
                    </div>
                </div>
                <div class="full_width">
                    <div class="card_progress">
                        <div class="progress_bar"><span class="progress" style="width: 10%"></span> <span class="value">10%</span></div>
                    </div>
                </div>
            </a>

            <a href="#card_link" class="card_box orange">
                <div class="full_width">
                    <div class="card_name">
                        <h3>Level 2</h3>
                    </div>
                </div>
                <div class="half_width">
                    <div class="card_points">
                        <span>points</span>
                        21
                    </div>
                </div>
                <div class="half_width">
                    <div class="card_questions">
                        <span>questions</span>
                        10/30
                    </div>
                </div>
                <div class="full_width">
                    <div class="card_progress">
                        <div class="progress_bar"><span class="progress" style="width: 50%"></span> <span class="value">50%</span></div>
                    </div>
                </div>
            </a>

            <a href="#card_link" class="card_box red">
                <div class="full_width">
                    <div class="card_name">
                        <h3>Level 3</h3>
                    </div>
                </div>
                <div class="half_width">
                    <div class="card_points">
                        <span>points</span>
                        21
                    </div>
                </div>
                <div class="half_width">
                    <div class="card_questions">
                        <span>questions</span>
                        10/30
                    </div>
                </div>
                <div class="full_width">
                    <div class="card_progress">
                        <div class="progress_bar"><span class="progress" style="width: 90%"></span> <span class="value">90%</span></div>
                    </div>
                </div>
            </a>

            <a href="#card_link" class="card_box green">
                <div class="full_width">
                    <div class="card_name">
                        <h3>Level 3</h3>
                    </div>
                </div>
                <div class="half_width">
                    <div class="card_points">
                        <span>points</span>
                        21
                    </div>
                </div>
                <div class="half_width">
                    <div class="card_questions">
                        <span>questions</span>
                        10/30
                    </div>
                </div>
                <div class="full_width">
                    <div class="card_progress">
                        <div class="progress_bar"><span class="progress" style="width: 90%"></span> <span class="value">90%</span></div>
                    </div>
                </div>
            </a>

            <a href="#card_link" class="card_box blue">
                <div class="full_width">
                    <div class="card_name">
                        <h3>Level 3</h3>
                    </div>
                </div>
                <div class="half_width">
                    <div class="card_points">
                        <span>points</span>
                        21
                    </div>
                </div>
                <div class="half_width">
                    <div class="card_questions">
                        <span>questions</span>
                        10/30
                    </div>
                </div>
                <div class="full_width">
                    <div class="card_progress">
                        <div class="progress_bar"><span class="progress" style="width: 90%"></span> <span class="value">90%</span></div>
                    </div>
                </div>
            </a>

            <a href="#card_link" class="card_box yellow">
                <div class="full_width">
                    <div class="card_name">
                        <h3>Level 3</h3>
                    </div>
                </div>
                <div class="half_width">
                    <div class="card_points">
                        <span>points</span>
                        21
                    </div>
                </div>
                <div class="half_width">
                    <div class="card_questions">
                        <span>questions</span>
                        10/30
                    </div>
                </div>
                <div class="full_width">
                    <div class="card_progress">
                        <div class="progress_bar"><span class="progress" style="width: 90%"></span> <span class="value">90%</span></div>
                    </div>
                </div>
            </a>

            <a href="#card_link" class="card_box locked">
                <div class="full_width">
                    <div class="card_name">
                        <h3>Level 3</h3>
                    </div>
                </div>
                <div class="half_width">
                    <div class="card_points">
                        <span>points</span>
                        0
                    </div>
                </div>
                <div class="half_width">
                    <div class="card_questions">
                        <span>questions</span>
                        0/40
                    </div>
                </div>
                <div class="full_width">
                    <div class="locked">
                        <i class="fa fa-lock"></i> Need a higher score to open.
                    </div>
                </div>
            </a>
        </div>


        <div id="about" class="section">
            <h2>Despre <strong>Quiz Game</strong></h2>
            <hr>

            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium adipisci animi aspernatur fugit
                ipsa iusto labore laudantium odio possimus praesentium quaerat, quas, repellendus sit sunt velit?
                Aliquid odio omnis temporibus?
            </p>

            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium adipisci animi aspernatur fugit
                ipsa iusto labore laudantium odio possimus praesentium quaerat, quas, repellendus sit sunt velit?
                Aliquid odio omnis temporibus?
            </p>
        </div>

        <div id="how" class="section">
            <h2>Regulile <strong>Jocului</strong></h2>
            <hr>

            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium adipisci animi aspernatur fugit
                ipsa iusto labore laudantium odio possimus praesentium quaerat, quas, repellendus sit sunt velit?
                Aliquid odio omnis temporibus?
            </p>

            <ol>
                <li>Rule 1 description 1</li>
                <li>Rule 2 description</li>
                <li>Rule 3 description</li>
                <li>Rule 4 description</li>
                <li>Rule 5 description</li>
            </ol>
        </div>
    </div>

<?php include_once 'footer.php'; ?>