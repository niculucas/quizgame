$(document).ready(function() {
    $('.menu_switcher').on('click touch', function (e) {
        e.preventDefault();

        $(this).toggleClass('active');
        $('#sidenav').toggleClass('active');
    });

    $(".sidenav a:not(.pop)").on('click touch', function(event) {
        event.preventDefault();
        $("html, body").animate({ scrollTop: $($(this).attr("href")).offset().top - 56 }, 600);

        $('.menu_switcher').toggleClass('active');
        $('#sidenav').toggleClass('active');
    });

    $(".play_btn.scroll_to").on('click touch', function(event) {
        event.preventDefault();
        // if( $(window).width() < 480 ){
        //     $("html, body").animate({ scrollTop: $($(this).attr("href")).offset().top - 90 }, 600);
        // }
    });


    $('.js_get_hint').on('click touch', function (e) {
        e.preventDefault();

        let parent = $(this).parents('.play_item');

        if(!$(this).hasClass('active')){
            playRewardSound();
        }

        $(this).addClass('active');
        parent.find('.hint').removeClass('hide');
    });

    $('.option').on('click touch', function (e) {
        e.preventDefault();

        let parent = $(this).parents('.play_item');

        if( $(this).data('value') === 1 ){
            $(this).addClass('success');

            parent.find('.play_success .info').removeClass('hide');
            parent.find('.play_errors .info').addClass('hide');

            playSuccessSound();
        }
        else{
            $(this).addClass('error');

            parent.find('.play_success .info').addClass('hide');
            parent.find('.play_errors .info').removeClass('hide');

            playErrorSound();
        }

        parent.find(".option").not(this).addClass("disabled");
    });

    $('.js_again').on('click touch', function (e) {
        e.preventDefault();

        if(!$(this).hasClass('active')){
            $(this).addClass('active');

            let parent = $(this).parents('.play_item');
                parent.find(".option").removeClass("disabled success error");
                parent.find('.hint').addClass('hide');
                parent.find('.play_success .info').addClass('hide');
                parent.find('.play_errors .info').addClass('hide');

            parent.find('.play_btn').not(this).removeClass('active');

            playRestartdSound();
        }
        else{
            $.amaran({
                'message'   :'Opțiunea nu poate fi utilizată!',
                'position'  :'top right'
            });
        }
    });

    // GamePlay slider
    let play_slider = $('#play_slider').slick({
        infinite: true,
        fade: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        dots: false,
        arrows: false,
        speed: 100,
       // draggable: false,
        touchMove: false
    });

    $(document).on('click touch', '.js_next_question', function (e) {
        e.preventDefault();
        play_slider.slick('slickNext');
    });

    $(document).on('click touch', '.js_prev_question', function (e) {
        e.preventDefault();
        play_slider.slick('slickPrev');
    });

    if( typeof $.fn.magnificPopup !== typeof undefined){
        $(".pop").magnificPopup({
            type:"inline",
            midClick: true,
            mainClass: 'default'
        });
    }

    var Password = {
        _pattern : /[a-zA-Z0-9_\-\+\.]/,

        _getRandomByte : function()
        {
            // http://caniuse.com/#feat=getrandomvalues
            if(window.crypto && window.crypto.getRandomValues)
            {
                var result = new Uint8Array(1);
                window.crypto.getRandomValues(result);
                return result[0];
            }
            else if(window.msCrypto && window.msCrypto.getRandomValues)
            {
                var result = new Uint8Array(1);
                window.msCrypto.getRandomValues(result);
                return result[0];
            }
            else
            {
                return Math.floor(Math.random() * 256);
            }
        },
        generate : function(length)
        {
            return Array.apply(null, {'length': length})
                .map(function()
                {
                    var result;
                    while(true)
                    {
                        result = String.fromCharCode(this._getRandomByte());
                        if(this._pattern.test(result))
                        {
                            return result;
                        }
                    }
                }, this)
                .join('');
        }
    };

    $(document).on('click touch', '.password_generator', function (e) {
        e.preventDefault();

        var input = $(this).parents('label').find('input'),
            output = $(this).parents('label').find('.password_display'),
            ps = Password.generate(8);

        input.val('');
        input.val(ps);
        output.text( ps );
    });

    function startTimer(duration, display) {
        if(display != null){
            let timer = duration, minutes, seconds;
            setInterval(function () {
                minutes = parseInt(timer / 60, 10);
                seconds = parseInt(timer % 60, 10);

                minutes = minutes < 10 ? "0" + minutes : minutes;
                seconds = seconds < 10 ? "0" + seconds : seconds;

                display.textContent = minutes + ":" + seconds;

                if (--timer < 0) {
                    timer = duration;
                }
            }, 1000);
        }
    }

    let display = document.querySelector('#play_timer');
    startTimer(30, display);

    // play sounds
    let successSound = document.createElement('audio');
    let errorSound   = document.createElement('audio');
    let rewardSound  = document.createElement('audio');
    let restartSound  = document.createElement('audio');

    successSound.setAttribute('src', '/sounds/success.mp3');
    errorSound.setAttribute('src', '/sounds/error.mp3');
    rewardSound.setAttribute('src', '/sounds/reward.mp3');
    restartSound.setAttribute('src', '/sounds/restart.mp3');

    let playSuccessSound  = function(){successSound.play()}
    let playErrorSound    = function(){errorSound.play()}
    let playRewardSound   = function(){rewardSound.play()}
    let playRestartdSound = function(){restartSound.play()}

    // play sounds

});
