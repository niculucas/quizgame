<!doctype html>
<html lang="ro">
<head>
    <meta charset="UTF-8"/>
    <title>Quiz Game</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <link rel="icon" href="images/favicon.png" type="image/png" sizes="64x64">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;600;800&display=swap" rel="stylesheet">

    <meta property="og:type" content="website" />
    <meta property="og:title" content="Quiz Game" />
    <meta property="og:image" content="http://mzlucas.ru/images/banner.jpg" />
    <meta property="og:description" content="Quiz Game - Joc interactiv pentru antreprenori." />
    <meta property="og:site_name" content="Quiz Game" />
</head>
<body>

<div id="reset_modal" class="modal_form mfp-hide">
    <div class="popup_entry">
        <form class="simple_form" action="index.php" method="post">
            <div class="server_response"></div>

            <div class="form_heading">
                <p>
                    <img class="dog" src="images/dog.png" alt="">
                </p>
                <h2>
                    <i class="fa fa-refresh"></i> Resetează parola
                </h2>
            </div>

            <label>
                <input type="text" name="email" value="" placeholder="E-mail" autocomplete="off" required="">
            </label>

            <div class="form_submit">
                <button type="submit" class="tap submit_button">Resetează</button>
            </div>
        </form>
    </div>
</div>

<div id="login_modal" class="modal_form mfp-hide">
    <div class="popup_entry">
        <form class="simple_form" action="index.php" method="post">
            <div class="server_response"></div>

            <div class="form_heading">
                <p>
                    <img class="dog" src="images/dog.png" alt="">
                </p>
                <h2>
                    <i class="fa fa-lock"></i> Loghează-te
                </h2>
            </div>

            <label>
                <input type="text" name="email" value="admin@quiz.com" placeholder="E-mail" autocomplete="off" required="">
            </label>

            <label>
                <input type="password" name="password" value="123456" placeholder="Parola" autocomplete="off" required="">
            </label>

            <div class="form_submit">
                <button type="submit" class="tap submit_button">Să începem!</button>
            </div>

            <div class="form_reset">
                Ai uitat parola? <a href="#reset_modal" class="pop">Solicită resetarea parolei.</a>
            </div>
        </form>
    </div>
</div>

<div id="register_modal" class="modal_form mfp-hide">
    <div class="popup_entry">
        <form class="simple_form" action="index.php" method="post">
            <div class="server_response"></div>

            <div class="form_heading">
                <p>
                    <img class="dog" src="images/dog.png" alt="">
                </p>
                <h2>
                    <i class="fa fa-lock"></i> Crează-ți un cont
                </h2>
            </div>

            <label>
                <span>Nume</span>
                <input type="text" name="name" value="" placeholder="Numele tău complet" required="">
            </label>

            <label>
                <span>E-mail</span>
                <input type="text" name="email" value="" placeholder="i.am.genius@quiz.com" required="">
            </label>

            <label>
                <span>Parola <span class="password_generator"><i class="fa fa-cog"></i> Generează</span></span>
                <input type="text" name="password" value="" placeholder="Generează o parolă sigură" autocomplete="off" required="">
            </label>

            <div class="form_submit">
                <button type="submit" class="tap submit_button">Crează-mi contul!</button>
            </div>
        </form>
    </div>
</div>

<div id="sidenav" class="sidenav">
    <div>
        <ul>
            <li><a href="#login_modal" class="pop"><i class="fa fa-sign-in"></i> Logare</a></li>
            <li><a href="#register_modal" class="pop"><i class="fa fa-user-plus"></i> Înregistrare</a></li>
            <li><a href="#about"><i class="fa fa-info-circle"></i> Despre joc</a></li>
            <li><a href="#how"><i class="fa fa-question-circle"></i> Regulile jocului</a></li>
        </ul>
    </div>
</div>

<div id="header" class="header">
    <div class="container">
        <div>
            <div class="logo_box">
                <a href="/"><img src="images/logo.png" alt="Quiz App"></a>
            </div>
        </div>
        <div>
            <div class="user_info">
                Scorul tău: <span id="user_score" class="user_score">29</span>
            </div>
        </div>
        <div>
            <div class="menu clearfix">
                <a href="#sidenav" class="menu_switcher">
                    <i class="fa fa-bars"></i>
                </a>
            </div>
        </div>
    </div>
</div>